package com.syyai.spring.boot.urule;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bstek.urule.KnowledgePackageReceiverServlet;
import com.bstek.urule.console.servlet.URuleServlet;

@Configuration
public class URuleBeanRegisration {
	
	
	static {
		System.out.println("load class :"+URuleBeanRegisration.class.getCanonicalName());
	}
	
	@Bean
    public ServletRegistrationBean<URuleServlet> registerURuleServlet(){
        return new ServletRegistrationBean<URuleServlet>(new URuleServlet(),"/urule/*");
    }
	
	
	@Bean
    public ServletRegistrationBean<KnowledgePackageReceiverServlet> registerKnowledgePackageReceiverServlet(){
        return new ServletRegistrationBean<KnowledgePackageReceiverServlet>(new KnowledgePackageReceiverServlet(),"/knowledgepackagereceiver");
    }
	
	
	
}