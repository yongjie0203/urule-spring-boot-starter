# urule-spring-boot-starter

#### 项目介绍
urule2 pro 试用版 springboot starter 

#### 软件架构
软件架构说明


#### 安装教程


  请在pom.xml中添加如下依赖即可完成自动配置
<dependency>
    <groupId>com.syyai.spring.boot</groupId>
    <artifactId>urule-spring-boot-starter</artifactId>
    <version>2.0.7</version>
</dependency>





#### 使用说明

请在pom.xml中添加如下依赖即可完成自动配置
<dependency>
    <groupId>com.syyai.spring.boot</groupId>
    <artifactId>urule-spring-boot-starter</artifactId>
    <version>2.0.7</version>
</dependency>

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
7. 如果需要urule pro版证书，请点击[获取urule pro版证书](http://120.27.54.60/info.html)